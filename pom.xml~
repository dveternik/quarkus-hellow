<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>io.sample</groupId>
	<artifactId>quarkus-hellow</artifactId>
	<version>0.0.1-SNAPSHOT</version>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

		<java.version>17</java.version>
		<java.version.required>17.0.2</java.version.required>

		<quarkus.version>3.4.3</quarkus.version>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>io.quarkus.platform</groupId>
				<artifactId>quarkus-bom</artifactId>
				<version>${quarkus.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<!-- quarkus -->
		<dependency>
			<groupId>org.jboss.logmanager</groupId>
			<artifactId>log4j2-jboss-logmanager</artifactId>
		</dependency>
		<dependency>
			<groupId>io.quarkus</groupId>
			<artifactId>quarkus-info</artifactId>
		</dependency>		
		<dependency>
			<groupId>io.quarkus</groupId>
			<artifactId>quarkus-container-image-jib</artifactId>
		</dependency>
	</dependencies>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-enforcer-plugin</artifactId>
					<version>3.4.1</version>
					<executions>
						<execution>
							<goals>
								<goal>enforce</goal>
							</goals>
						</execution>
					</executions>
					<configuration>
						<fail>true</fail>
						<failFast>true</failFast>
						<rules>
							<requireMavenVersion>
								<version>[3.9.1, 4)</version>
							</requireMavenVersion>
							<requireJavaVersion>
								<version>[${java.version.required}, 18)</version>
							</requireJavaVersion>
						</rules>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.11.0</version>
					<configuration>
						<source>${java.version}</source>
						<target>${java.version}</target>
						<compilerArgs>
							<arg>-parameters</arg>
						</compilerArgs>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>3.3.0</version>
					<configuration>
						<archive>
							<manifest>
								<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
							</manifest>
							<manifestEntries>
								<Implementation-Vendor-Id>${project.groupId}</Implementation-Vendor-Id>
								<X-Git-Branch>${git.branch}</X-Git-Branch>
								<X-Git-Commit-Id>${git.commit.id}</X-Git-Commit-Id>
								<X-Git-Dirty>${git.dirty}</X-Git-Dirty>
								<X-Build-Timestamp>${maven.build.timestamp}</X-Build-Timestamp>
								<X-Maven-Gav>${project.groupId}:${project.artifactId}:${project.packaging}:${project.version}</X-Maven-Gav>
							</manifestEntries>
						</archive>
					</configuration>
				</plugin>
				<plugin>
					<groupId>io.github.git-commit-id</groupId>
					<artifactId>git-commit-id-maven-plugin</artifactId>
					<version>6.0.0</version>
					<executions>
						<execution>
							<goals>
								<goal>revision</goal>
							</goals>
						</execution>
					</executions>
					<configuration>
						<runOnlyOnce>true</runOnlyOnce>
						<generateGitPropertiesFile>false</generateGitPropertiesFile>
						<includeOnlyProperties>
							<includeOnlyProperty>git.branch</includeOnlyProperty>
							<includeOnlyProperty>git.commit.id</includeOnlyProperty>
							<includeOnlyProperty>git.dirty</includeOnlyProperty>
						</includeOnlyProperties>
					</configuration>
				</plugin>				
				<plugin>
					<groupId>io.quarkus.platform</groupId>
					<artifactId>quarkus-maven-plugin</artifactId>
					<version>${quarkus.version}</version>
					<extensions>true</extensions>
					<executions>
						<execution>
							<goals>
								<goal>build</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
			</plugins>
		</pluginManagement>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>io.github.git-commit-id</groupId>
				<artifactId>git-commit-id-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>io.quarkus.platform</groupId>
				<artifactId>quarkus-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>
</project>
