# quarkus-hellow

```
mvn clean verify
docker run -p8080:8080 sample/quarkus-hellow:0.0.1-SNAPSHOT
```

Check if container is running:
```
http://localhost:8080/q/info
```